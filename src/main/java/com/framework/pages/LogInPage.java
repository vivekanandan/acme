package com.framework.pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class LogInPage extends ProjectMethods{
	
	public LogInPage() {
		
		PageFactory.initElements(driver, this);
	}
    
	@FindBy(how=How.ID,using="email") WebElement email;
	@FindBy(how=How.ID,using="password") WebElement password;
	@FindBy(how=How.XPATH,using="//button[@id='buttonLogin']") WebElement login;
	
	public LogInPage EnterEmailID(String emailId) {
		
	clearAndType(email, emailId);	
		return this;
	}
	
	public LogInPage EnterPassword(String pass) {
		
		clearAndType(password, pass);	
			return this;
		}
	public HomePage ClickLogIn()
	{
		
		click(login);
		
		return new HomePage();
	}
		
	
}

package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


import com.framework.design.ProjectMethods;

public class HomePage extends ProjectMethods {

	public HomePage() {
		
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how=How.XPATH,using="//i[@class='fa fa-truck']/..") WebElement vendor;
	@FindBy(how=How.XPATH,using="//i[@class='fa fa-truck']/following::ul/li/a[text()='Search for Vendor']") WebElement searchvendor;
		
	
	public SearchVendor ClickVendors() {
		
		MouseAction(vendor, searchvendor);
		
		return new SearchVendor();
	}
	
}

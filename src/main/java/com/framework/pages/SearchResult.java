package com.framework.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class SearchResult extends ProjectMethods {

	public SearchResult() {
		// TODO Auto-generated constructor stub
		
		PageFactory.initElements(driver, this);
	}

	public SearchResult getName() {
		
       WebElement table1 = driver.findElementByClassName("table");
		
		List<WebElement> row = table1.findElements(By.tagName("tr"));
		
		List<WebElement> column = row.get(1).findElements(By.tagName("td"));
	    
		String vendorname=column.get(0).getText();
		
		System.out.println(vendorname);
	    
		return this;
	}
	
}

package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class SearchVendor extends ProjectMethods{

	public SearchVendor() {
	
	PageFactory.initElements(driver, this);
	}

	@FindBy (how=How.ID,using="vendorTaxID") WebElement typeID;
	@FindBy (how=How.ID,using="buttonSearch") WebElement clickbutton;
	
	public SearchVendor Search(String Id) {
		clearAndType(typeID, Id);
		return this;
	}
	
	public SearchResult clickSearchButton() {
		
		click(clickbutton);
		
		return new SearchResult();
	}
	
}

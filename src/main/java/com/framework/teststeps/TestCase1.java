package com.framework.teststeps;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LogInPage;

public class TestCase1 extends ProjectMethods{

	@BeforeTest
	public void setdata() {
		
		testCaseName="TestCase1";
		testDescription="Search Vendor";
		testNodes="Vendors";
		author="Vivek";
		category="smoke";
		dataSheetName="TC001";
		
	}
	@Test(dataProvider="fetchData")
	public void login(String emailId,String pass,String Id) {
		
		new LogInPage()
		.EnterEmailID(emailId)
		.EnterPassword(pass)
		.ClickLogIn()
		.ClickVendors()
		.Search(Id)
		.clickSearchButton()
		.getName();
	}
	
}

package driverSteps;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LogInACME {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver=new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://acme-test.uipath.com/account/login");
		
		driver.findElementById("email").sendKeys("ev.anand07@gmail.com");
		driver.findElementById("password").sendKeys("vivek11");
		
		driver.findElementByXPath("//button[@id='buttonLogin']").click();
		
			
		WebElement element = driver.findElementByXPath("//i[@class='fa fa-truck']/..");
		
		WebElement searchElement = driver.findElementByXPath("//i[@class='fa fa-truck']/following::ul/li/a[text()='Search for Vendor']");
		
		Actions builder =new Actions(driver);
		builder.moveToElement(element).pause(2000).perform();
		builder.click(searchElement).perform();
		
		driver.findElementById("vendorTaxID").sendKeys("RO874231");
		driver.findElementById("buttonSearch").click();
		
		//String vendorname=driver.findElementByXPath("//table[@class='table']/tbody/tr[2]/td[1]").getText();
	
		WebElement table1 = driver.findElementByClassName("table");
		
		List<WebElement> row = table1.findElements(By.tagName("tr"));
		
		List<WebElement> column = row.get(1).findElements(By.tagName("td"));
	    
		String vendorname=column.get(0).getText();
		
		System.out.println(vendorname);
	    
		driver.close();
	    
	
	}

}
